Ext.define('LO.view.main.MainPanel', {
  extend: 'CM.view.main.Main',
  xtype: 'app-main',
  config: {
    headerTitle: 'Operations Management',
    footerText: 'Operations Management'
  },
  itemId: 'mainViewport'
});
