/**
 * The main application class. An instance of this class is created by app.js when it calls
 * Ext.application(). This is the ideal place to handle application launch and initialization
 * details.
 */
Ext.define('LO.Application', {
    extend: 'Ext.app.Application',
    requires: [
        'CM.view.login.Login',
        'CM.overrides.tree.ColumnOverride',
        'CM.util.CsrfTokenHelper',
        'CM.util.Glyphs',
        'CM.util.Util',
        'CM.model.login.Login',
        'LO.view.main.MainPanel'
    ],
    glyphFontFamily: 'FontAwesome',
    name: 'PH',

    controllers: [
        'Menu'
    ],

    stores: [
        // TODO: add global / shared stores here
    ],

    launch: function () {
        var label;
        Ext.Ajax.request({
            url: '/common/check_login',
            success: function (conn) {
                var result = CM.util.Util.decodeJSON(conn.responseText);
                if (result.success) {
                    Ext.create('PH.view.main.MainPanel');
                    label = Ext.ComponentQuery.query('container appheader label#userLabel')[0];
                    label.setHtml('<span style="font-size: 12px">Logged in user: <b>' + result.data + '</b></span>');
                } else {
                    var me = this;
                    me.splashscreen = Ext.getBody().mask(
                      'Loading Application', 'splashscreen'
                    );
                    Ext.DomHelper.insertFirst(Ext.query('.x-mask-msg')[0], {
                        cls: 'x-splash-icon'
                    });
                    Ext.Ajax.request({
                        url: '/common/csrf_token',

                        success: function () {
                            var csrftoken = Ext.util.Cookies.get('csrftoken')
                        }
                    });

                    var task = new Ext.util.DelayedTask(function () {
                        me.splashscreen.fadeOut({
                            duration: 1000,
                            remove: true,
                            listeners: {
                                afteranimate: function () {
                                    Ext.widget('login-window');
                                }
                            }
                        });
                    });
                    task.delay(2000);
                }
            }
        });
    }
});
