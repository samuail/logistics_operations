/**
 * Created by betse on 6/5/2015.
 */
Ext.define('LO.controller.Menu', {
  extend: 'CM.controller.menu.BaseMenuController',
  requires: ['CM.store.menu.Menu'],
  stores: [
    'CM.store.menu.Menu',
    'CM.view.user.UserGrid'
  ],
  views: [

  ]
});
